// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  appVersion: 'development',
  firebase: {
    apiKey: 'AIzaSyCqDJ-XzQaMIVyHwJUsL9jt3MKNGhyxkHY',
    authDomain: 'amigodobolso2test.firebaseapp.com',
    databaseURL: 'https://amigodobolso2test.firebaseio.com',
    projectId: 'amigodobolso2test',
    storageBucket: 'amigodobolso2test.appspot.com',
    messagingSenderId: '363513979129',
    appId: '1:363513979129:web:236fd2c12196d37d'
  }
};
