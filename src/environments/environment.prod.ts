export const environment = {
  production: true,
  appVersion: 'v0.1.20-alpha (04-06-2019 15:48)',
  firebase: {
    apiKey: 'AIzaSyCqDJ-XzQaMIVyHwJUsL9jt3MKNGhyxkHY',
    authDomain: 'amigodobolso2test.firebaseapp.com',
    databaseURL: 'https://amigodobolso2test.firebaseio.com',
    projectId: 'amigodobolso2test',
    storageBucket: 'amigodobolso2test.appspot.com',
    messagingSenderId: '363513979129',
    appId: '1:363513979129:web:236fd2c12196d37d'
  }
};
