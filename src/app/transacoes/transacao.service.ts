import { Injectable } from '@angular/core';
import { Transacao } from '../shared/models/transacao';

import { Response } from '@angular/http';

import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { AuthService } from '../shared/services/auth.service';
import { Observable, BehaviorSubject } from 'rxjs';

import 'rxjs/add/observable/combineLatest';
import 'rxjs/operators/switchMap';

import * as firebase from 'firebase/app';
import { switchMap, map } from 'rxjs/operators';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

@Injectable()
export class TransacaoService {

  public mesCorrente = new Date().getMonth() + 1;
  public anoCorrente = new Date().getFullYear();

  private transacaoCollectionRef: AngularFirestoreCollection<any>;
  transacoes$: Observable<Object[]>;

  monthYearFilter$: BehaviorSubject<any | null> = new BehaviorSubject(null);

  constructor(private http: HttpClient, private afs: AngularFirestore, private authService: AuthService) {
    this.transacaoCollectionRef = this.afs.doc(`usuarios/${this.authService.loggedUser.uid}`).collection('transacoes');

    this.monthYearFilter$ = new BehaviorSubject({ year: new Date().getFullYear(), month: new Date().getMonth() });

    this.transacoes$ = Observable.combineLatest(
      this.monthYearFilter$
    ).pipe(
      switchMap(([monthYearFilter]) =>

        this.afs.doc(`usuarios/${this.authService.loggedUser.uid}`)
          .collection('transacoes', ref => {
            let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

            if (monthYearFilter) {
              query = query.where('data', '>=', new Date(monthYearFilter['year'], monthYearFilter['month'], 1))
                .where('data', '<=', new Date(monthYearFilter['year'], monthYearFilter['month'] + 1, 0));
            }
            return query;

          }).snapshotChanges().pipe(
            map(actions => {
              return actions.map(action => {
                const data = action.payload.doc.data();
                data['data'] = data['data'].toDate();
                const id = action.payload.doc.id;
                return { id, ...data };
              });
            })
          )
      )
    );
  }

  // get("/api/transacoes")
  getTransacoes(mes, ano) {
    this.monthYearFilter$.next({ year: ano, month: mes });
  }

  getTransacao(id): Observable<any> {
    return this.transacaoCollectionRef.doc(id).valueChanges();
  }

  createTransacao(newTransacao): Promise<any> {
    return this.transacaoCollectionRef.add(newTransacao);
  }

  updateTransacao(transacao: Object): Promise<void | Transacao> {
    const id = transacao['id'];
    delete transacao['id'];

    return this.transacaoCollectionRef.doc(id).update(transacao);
  }

  deleteTransacao(idTransacao: string): Promise<void | String> {
    return this.transacaoCollectionRef.doc(idTransacao).delete();
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
  }

  setMonthYear(newMonth, newYear) {
    this.mesCorrente = newMonth;
    this.anoCorrente = newYear;
    this.getTransacoes(this.mesCorrente - 1, this.anoCorrente);
  }

  nextMonth(): void {
    if (this.mesCorrente < 12) {
      this.mesCorrente++;
    } else {
      this.mesCorrente = 1;
      this.anoCorrente++;
    }
    this.getTransacoes(this.mesCorrente - 1, this.anoCorrente);
  }

  previousMonth(): void {
    if (this.mesCorrente > 1) {
      this.mesCorrente--;
    } else {
      this.mesCorrente = 12;
      this.anoCorrente--;
    }
    this.getTransacoes(this.mesCorrente - 1, this.anoCorrente);
  }


}
