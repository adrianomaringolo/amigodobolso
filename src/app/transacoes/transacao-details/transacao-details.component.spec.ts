import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransacaoDetailsComponent } from './transacao-details.component';

describe('TransacaoDetailsComponent', () => {
  let component: TransacaoDetailsComponent;
  let fixture: ComponentFixture<TransacaoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransacaoDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransacaoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
