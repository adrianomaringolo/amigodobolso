import { Component, OnInit, AfterViewChecked, AfterContentChecked } from '@angular/core';
import { TransacaoService } from '../transacao.service';
import { Transacao } from '../../shared/models/transacao';
import { MzToastService } from 'ngx-materialize';
import { Router } from '@angular/router';
import { LoadingService } from '../../shared/services/loading.service';
import { NavigationService } from '../../shared/services/navigation.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

import * as moment from 'moment';

@Component({
  selector: 'app-transacao-list',
  templateUrl: './transacao-list.component.html',
  styleUrls: ['./transacao-list.component.scss']
})
export class TransacaoListComponent implements OnInit, AfterContentChecked {

  public transacoes;

  public somaEntradas = 0;
  public somaSaidas = 0;

  public hasNoItens = false;

  public selectMonth = false;
  datesGrouped = [];

  public meses = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Março',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
  };

  public modalOptions: Materialize.ModalOptions = {
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '30%', // Ending top style attribute
  };

  constructor(public transacaoService: TransacaoService, private _toastService: MzToastService, private _router: Router,
    private _loadingService: LoadingService, private _navService: NavigationService, public ngxSmartModalService: NgxSmartModalService) {
    this.transacaoService.transacoes$.subscribe(
      res => {
        this.transacoes = res;
        this.groupByDate(res);
      }
    );
    this._navService.isInList = true;
  }

  ngOnInit() {
    moment.locale('pt-br');
  }

  onMonthChange(event: { monthIndex: number, year: number }) {
    this.transacaoService.setMonthYear(event.monthIndex + 1, event.year);
    this.ngxSmartModalService.getModal('myModal').close();
  }

  ngAfterContentChecked() {
    this._loadingService.stopLoading();
  }

  getCategoriaName(categoria): string {
    return Transacao.CategoriasLabels[categoria] || 'Receita';
  }

  getCategoriaIcon(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].icon : 'attach_money';
  }

  getCategoriaColor(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].color : '#4bb35f';
  }

  getValorAbs(transacao) {
    return Math.abs(transacao.valor);
  }

  editarTransacao(id): void {
    this._router.navigate(['transacoes/editar/', id]);
  }

  showTransacaoDetails(transacao) {
    this.ngxSmartModalService.resetModalData('modalDetails');
    this.ngxSmartModalService.setModalData({ transacao: transacao }, 'modalDetails');
    this.ngxSmartModalService.getModal('modalDetails').open();
  }

  getDateString(date) {
    let dateToShow = moment(date);
    return dateToShow.date() + ' de ' + moment.months()[dateToShow.month()] + ', ' + moment.weekdays()[dateToShow.weekday()];
  }

  getFormaPagamento(transacao) {
    if (transacao.forma === 'vista') {
      return 'À vista'
    } else if (transacao.forma === 'parcelado') {
      return `x ${transacao.numParcelas} parcelas`;
    }

    return '';
  }

  getDates() {
    return Object.keys(this.datesGrouped);
  }

  groupByDate(lancamentos) {

    let auxDatesGrouped = Object.create(null);

    this.somaEntradas = 0;
    this.somaSaidas = 0;

    lancamentos.forEach(transacao => {
      const data = transacao.data.toDateString();
      const dataStr = new Date(data).toISOString();
      if (auxDatesGrouped[dataStr]) {
        auxDatesGrouped[dataStr]['transacoes'].push(transacao);
      } else {
        auxDatesGrouped[dataStr] = { 'transacoes': [transacao], 'data': new Date(data) };
      }

      if (transacao.valor > 0) {
        this.somaEntradas += transacao.valor;
      } else {
        this.somaSaidas += transacao.valor;
      }
    });

    this.datesGrouped = Object.create(null);

    let saldoTotal = 0;

    Object.keys(auxDatesGrouped)
      .sort()
      .forEach((v, i) => {
        this.datesGrouped[v] = auxDatesGrouped[v];

        auxDatesGrouped[v].transacoes.forEach(t => {
          saldoTotal += t.valor;
        });

        this.datesGrouped[v]['saldo'] = saldoTotal;
      });
  }

  confirmaExcluirTransacao(idToRemove): void {
    this._loadingService.startLoading();

    this.transacaoService.deleteTransacao(idToRemove).then((response: String) => {
      this._router.navigate(['transacoes']);
      this._toastService.show(`Transação excluída!`, 4000);
      this._loadingService.stopLoading();
      this.ngxSmartModalService.getModal('modalDetails').close();
    });
  }

  getTransacaoEfetivada(transacao) {
    if (transacao.efetivado === undefined || transacao.efetivado == true) {
      return true;
    }

    return false;
  }

  isToday(date): boolean {
    return false; //new Date().setHours(0, 0, 0, 0) === date.setHours(0, 0, 0, 0);
  }

}
