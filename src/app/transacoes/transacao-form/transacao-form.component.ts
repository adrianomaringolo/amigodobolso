import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Transacao } from '../../shared/models/transacao';
import { TransacaoService } from '../transacao.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MzToastService } from 'ngx-materialize';
import { AuthService } from '../../shared/services/auth.service';
import { LoadingService } from '../../shared/services/loading.service';

@Component({
  selector: 'app-transacao-form',
  templateUrl: './transacao-form.component.html',
  styleUrls: ['./transacao-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TransacaoFormComponent {

  private transacao: Transacao;

  public valor: number;
  public descricao = '';
  public obs = '';
  public data: Date = new Date();
  public categoria = '';
  public formaPagamento = 'vista';
  public numParcelas = 2;
  public periodo = '';

  public efetivado = true;

  public tipoTransacao = 'despesa';

  public categoriasOptions = [
    { value: 'necessidades_essenciais', name: 'Necessidades essenciais', icon: 'shopping-basket' },
    { value: 'tranquilidade_financeira', name: 'Tranquilidade financeira', icon: 'sun' },
    { value: 'fazer_pelo_outro', name: 'Fazer pelo outro', icon: 'favorite' },
    { value: 'diversao', name: 'Diversão', icon: 'smiley' },
    { value: 'compras_longo_prazo', name: 'Compras de longo prazo', icon: 'clock' },
    { value: 'desenvolvimento_pessoal', name: 'Desenvolvimento Pessoal', icon: 'people' },
  ];

  public formasPagamentoOptions = [
    { value: 'vista', name: 'À vista' },
    { value: 'parcelado', name: 'Parcelado' }
    // { value: 'fixo', name: 'Periódico' }
  ];

  public periodosOptions = [
    { value: 'mes', name: 'Mensal' },
    { value: 'quinzena', name: 'Quinzenal' },
    { value: 'semana', name: 'Semanal' }
  ];

  public dateOptions: Pickadate.DateOptions = {
    labelMonthNext: 'Mês seguinte',
    labelMonthPrev: 'Mês anterior',
    labelMonthSelect: 'Selecionar mês',
    labelYearSelect: 'Selecionar ano',
    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
      'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    weekdaysFull: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
    weekdaysShort: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    today: 'Hoje',
    clear: 'Limpar',
    close: 'OK',
    format: 'dd/mm/yyyy',
    formatSubmit: 'yyyy-mm-dd',
    closeOnSelect: true
  };


  constructor(private _transacaoService: TransacaoService, private _toastService: MzToastService,
    private _router: Router, private activateRoute: ActivatedRoute, private _authService: AuthService,
    private _loadingService: LoadingService) {

    this.activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._loadingService.startLoading();
          this._transacaoService.getTransacao(params['id']).subscribe(res => {
            this.transacao = res;
            this.transacao.id = params['id'];

            this.descricao = this.transacao.descricao;
            this.valor = this.transacao.valor;
            this.data = this.transacao.data;
            this.formaPagamento = this.transacao.forma;
            this.periodo = this.transacao.periodo;
            this.numParcelas = this.transacao.numParcelas;
            this.categoria = this.transacao.categoria;
            this.obs = this.transacao.obs;

            if (this.transacao.efetivado === undefined) {
              this.efetivado = true;
            } else {
              this.efetivado = this.transacao.efetivado;
            }
            this.tipoTransacao = this.valor > 0 ? 'receita' : 'despesa';

            this._loadingService.stopLoading();
          });
        }
      });
      this.activateRoute.queryParams.subscribe(
        qparams => {
          if (qparams['mod']) {
            this.tipoTransacao = qparams['mod'];
          }
        });
  }

  getCategoriaIcon(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].icon : 'attach_money';
  }

  getCategoriaColor(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].color : '#4bb35f';
  }

  saveTransacao(isDespesa) {

    this._loadingService.startLoading();

    const newTransacao = {};
    newTransacao['descricao'] = this.descricao;
    newTransacao['valor'] = isDespesa ? -Math.abs(this.valor) : Math.abs(this.valor);

    let dtArray;

    try {
      dtArray = new Date(this.data).toISOString().split('T')[0].split('-');
    } catch {
      dtArray = new Date(this.data['toMillis']()).toISOString().split('T')[0].split('-');
    }
    newTransacao['data'] = new Date( Number(dtArray[0]), Number(dtArray[1]) - 1, Number(dtArray[2]));

    newTransacao['forma'] = this.formaPagamento;
    newTransacao['periodo'] = this.periodo;
    newTransacao['numParcelas'] = this.numParcelas;
    newTransacao['parcela'] = 1;
    newTransacao['categoria'] = isDespesa ? this.categoria : '';

    newTransacao['obs'] = this.obs || '';
    newTransacao['efetivado'] = this.efetivado || false;

    if (this.transacao && this.transacao.id) {
      newTransacao['id'] = this.transacao.id;
      this._transacaoService.updateTransacao(newTransacao).then((response: Transacao) => {
        this._router.navigate(['transacoes']);
        this._toastService.show(`${newTransacao['valor'] > 0 ? 'Receita' : 'Despesa'} atualizada!`, 4000);
      });
    } else {
      const desc = newTransacao['descricao'];

      if (newTransacao['forma'] === 'parcelado') {
        newTransacao['descricao'] = `${desc} (1/${newTransacao['numParcelas']})`;
      }

      this._transacaoService.createTransacao(newTransacao).then((response: Transacao) => {
        this._router.navigate(['transacoes']);
        this._toastService.show(`${newTransacao['valor'] > 0 ? 'Receita' : 'Despesa'} lançada!`, 4000);

        if (newTransacao['forma'] === 'parcelado') {

          for (let index = 2; index <= newTransacao['numParcelas']; index++) {
            newTransacao['parcela'] = index;
            newTransacao['descricao'] = `${desc} (${index}/${newTransacao['numParcelas']})`;
            newTransacao['data'] = new Date(newTransacao['data']);
            newTransacao['data'].setMonth(newTransacao['data'].getMonth() + 1);
            this._transacaoService.createTransacao(newTransacao).then();
          }
        }
      });
    }
  }

  isDespesa(): boolean {
    if (this.transacao && this.transacao.valor > 0) {
      return false;
    }
    return true;
  }

}
