import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransacaoListComponent } from './transacao-list/transacao-list.component';
import { TransacaoFormComponent } from './transacao-form/transacao-form.component';
import { SharedModule } from '../shared/shared.module';

import { TransacaoService } from './transacao.service';
import { routing } from '../app.routes';
import { TransacaoDetailsComponent } from './transacao-details/transacao-details.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    TransacaoListComponent,
    TransacaoFormComponent,
    TransacaoDetailsComponent
  ],
  providers: [
    TransacaoService
  ]
})
export class TransacoesModule { }
