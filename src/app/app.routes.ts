
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransacaoFormComponent } from './transacoes/transacao-form/transacao-form.component';
import { LoginComponent } from './login/login.component';
import { TransacaoListComponent } from './transacoes/transacao-list/transacao-list.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { AuthGuard } from './shared/auth.guard';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { HelpComponent } from './help/help.component';
import { PoliticaPrivacidadeComponent } from './help/politica-privacidade/politica-privacidade.component';

const APP_ROUTES: Routes = [
  {
    path: 'inicio',
    component: DashboardComponent, canActivate: [AuthGuard]
  },
  // Módulo de INÍCIO
  {
    path: 'transacoes',
    component: TransacaoListComponent, canActivate: [AuthGuard]
  },
  {
    path: 'transacoes/nova',
    component: TransacaoFormComponent, canActivate: [AuthGuard]
  },
  {
    path: 'transacoes/editar/:id',
    component: TransacaoFormComponent, canActivate: [AuthGuard]
  },
  {
    path: 'relatorios',
    component: RelatorioComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajuda',
    component: HelpComponent, canActivate: [AuthGuard]
  },
  {
    path: 'politica-privacidade',
    component: PoliticaPrivacidadeComponent
  },
  {
    path: 'preferencias-usuario',
    component: UserProfileComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: '/inicio',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/inicio'
  }
];

export const routing = RouterModule.forRoot(APP_ROUTES);
