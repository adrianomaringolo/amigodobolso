import { Component, OnInit, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-categoria-details',
  templateUrl: './categoria-details.component.html',
  styleUrls: ['./categoria-details.component.scss']
})
export class CategoriaDetailsComponent implements OnInit {

  private _value: number = 0;
  @Input() maxValue = 0;
  @Input() currencyValue = 0;
  @Input() availableValue = 0;
  @Input() nome: string;
  @Input() itens: any[] = [];

  leftValue = '0';

  valueUpdated: EventEmitter<any> = new EventEmitter();

  @Input()
  public set currentValue(value: number) {
    this._value = value;
    this.valueUpdated.emit(this._value);

    this.leftValue = `calc(${this.calcPercentage()} - 20px)`;
  }

  public get currentValue(): number {
    return this._value;
  }

  constructor() {}

  ngOnInit() {}

  onSelect(event) {
    console.log(event);
  }

  public getBadgeClass(): string {
    const percentage = (this.currentValue / this.maxValue) * 100;

    if (percentage <= 35) {
      return 'status-green';
    }

    if (percentage <= 65) {
      return 'status-yellow';
    }

    if (percentage <= 90) {
      return 'status-orange';
    }

    return 'status-red';
  }

  getValorAbs(transacao) {
    return Math.abs(transacao.valor);
  }

  public getCategoryMessage(): string {
    const percentage = (this.currentValue / this.maxValue) * 100;

    if (percentage <= 35) {
      return 'Oba! Você ainda pode gastar aqui!';
    }

    if (percentage <= 65) {
      return 'Bom! Gaste mais aqui, mas com responsabilidade';
    }

    if (percentage <= 90) {
      return 'Cuidado! Você está chegando no limite para o mês';
    }

    return 'Ops! Você já gastou mais do que podia por aqui!';
  }


  private calcPercentage(): string {
    if (this.currentValue >= this.maxValue) {
      return '100%';
    }

    if (this.currentValue === 0) {
      return '0%';
    }

    return `${(this.currentValue / this.maxValue) * 100}%`;
  }

}
