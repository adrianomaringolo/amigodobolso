


import { Component, OnInit } from '@angular/core';
import { TransacaoService } from '../transacoes/transacao.service';
import { MzToastService } from 'ngx-materialize';
import { Router, ActivatedRoute } from '@angular/router';
import { Transacao } from '../shared/models/transacao';
import { Observable } from 'rxjs';
import { NavigationService } from '../shared/services/navigation.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})
export class RelatorioComponent implements OnInit {

  public dadosGraficoPizza = [];
  public dadosGraficoCategorias = [];

  public receitaTotal = 0;

  public transacoes: Observable<any[]>;

  public CATEGORIAS = Transacao.Categorias;

  tipoGrafico = 'despesas-categoria';

  // esquema de cores das categorias principais
  colorScheme = {
    domain: Object.keys(Transacao.CategoriasIcons).map(key => Transacao.CategoriasIcons[key].color)
  };

  // pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  public meses = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Março',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
  };

  public somasCategorias = [];
  public totalCategorias: number;

  private transacoesPorCategoria: Object;

  constructor(public transacaoService: TransacaoService, private _toastService: MzToastService, private _router: Router,
    private _navService: NavigationService, public ngxSmartModalService: NgxSmartModalService, private activateRoute: ActivatedRoute) {
    this.transacoes = this.transacaoService.transacoes$;
    this.getDados();

    this._navService.isInList = false;

    this.activateRoute.queryParams.subscribe(
      qparams => {
        if (qparams['tipo']) {
          this.tipoGrafico = qparams['tipo'];
        }
      });
  }

  ngOnInit() {}

  onSelect(event) {}

  onMonthChange(event: { monthIndex: number, year: number }) {
    this.transacaoService.setMonthYear(event.monthIndex + 1, event.year);
    this.ngxSmartModalService.getModal('myModal').close();
  }

  getDados(): void {

    this.transacoes.subscribe(transacoes => {
      this.somasCategorias = [];
      this.dadosGraficoPizza = [];
      this.dadosGraficoCategorias = [];
      this.totalCategorias = 0;
      this.receitaTotal = 0;
      this.transacoesPorCategoria = {};

      // receitas
      transacoes.forEach(t => {
        if (t.valor > 0) { this.receitaTotal += t.valor; }
      });

      Transacao.getCategorias().forEach(c => {
        let valSoma = 0;
        const itens = [];
        transacoes.forEach(t => {
          if (t.categoria === c) {
            valSoma += t.valor;
            itens.push(t);
          }
        });

        this.totalCategorias += Math.abs(valSoma);
        this.transacoesPorCategoria[c] = itens;
        this.dadosGraficoPizza.push({ 'name': Transacao.getCategoriaName(c), 'value': Math.abs(valSoma) });
        this.dadosGraficoCategorias[c] = Math.round(Math.abs(valSoma / (this.receitaTotal === 0 ? 1 : this.receitaTotal)) * 100);
        this.somasCategorias.push({ categoria: c, soma: Math.abs(valSoma)});
      });

      this.somasCategorias = this.somasCategorias.sort((a, b) => ((a.soma < b.soma) ? 1 : -1));
    });
  }

  getCategoriaName(categoria): string {
    return Transacao.CategoriasLabels[categoria];
  }

  getCategoriaIcon(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].icon : 'attach_money';
  }

  getCategoriaColor(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].color : '#4bb35f';
  }

  getCategoriaPercent(value): number {
    return Math.abs(value / this.totalCategorias);
  }

  getItensCategoria(categoria): Transacao[] {
    return this.transacoesPorCategoria ? this.transacoesPorCategoria[categoria] : [];
  }

  getSomaCategoria(categoria): number {
    const item = this.somasCategorias.find(c => c.categoria === categoria);
    return item ? item.soma : 0;
  }

  getSaldoCategoria(categoria): number {
    const item = this.somasCategorias.find(c => c.categoria === categoria);
    const valorDisponivel = this.receitaTotal * (this.getCategoriaMaxValue(categoria) / 100);
    return valorDisponivel - item.soma;
  }

  getColorScheme(value, maxValue) {
    if (value >= maxValue) {
      return { domain: ['#d21e1e'] };
    }

    if (value > (3 * (maxValue / 4))) {
      return { domain: ['#C7B42C'] };
    }

    return { domain: ['#5AA454'] };
  }

  getCategoriaMaxValue(categoria): number {
    switch (categoria) {
      case Transacao.Categorias.NecessidadesEssenciais:
        return 55;
      case Transacao.Categorias.TranquilidadeFinanceira:
        return 10;
      case Transacao.Categorias.FazerPeloOutro:
        return 5;
      case Transacao.Categorias.Diversao:
        return 10;
      case Transacao.Categorias.ComprasDeLongoPrazo:
        return 10;
      case Transacao.Categorias.DesenvolvimentoPessoal:
        return 10;
      default:
        return 0;
    }
  }

  formatValue(value) {
    return `${value}%`;
  }
}
