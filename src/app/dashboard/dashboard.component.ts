import { Component, OnInit } from '@angular/core';
import { TransacaoService } from '../transacoes/transacao.service';
import { NavigationService } from '../shared/services/navigation.service';

import * as moment from 'moment';
import { Transacao } from '../shared/models/transacao';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public tipoResumo = 'mensal';

  public somaEntradas = 0;
  public somaSaidas = 0;

  public somasCategorias = [];
  public maiorCategoria;

  public dadosGraficoPizza = [];
  public dadosGraficoCategorias = [];
  public receitaTotal = 0;
  public totalCategorias: number;
  private transacoesPorCategoria: Object;

  public transacoesPendentes = [];

  public maioresTransacoes = [];

  // esquema de cores das categorias principais
  colorScheme = {
    domain: Object.keys(Transacao.CategoriasIcons).map(key => Transacao.CategoriasIcons[key].color)
  };


  transacoes = [];

  public tiposResumoOptions = [

    { value: 'semanal', name: 'Semanal' },
    { value: 'mensal', name: 'Mensal' },
    { value: 'anual', name: 'Anual' }
  ];

  constructor(public transacaoService: TransacaoService, private _navService: NavigationService) {
    this.transacaoService.transacoes$.subscribe(
      res => {
        this.transacoes = res;
        this.somaTotais(res);
        this.somaCategorias(res);
        this.getMaioresTransacoes(res);
        this.getTransacoesPendentes(res);
      }
    );
    this._navService.isInList = false;
  }

  ngOnInit() {
    moment.locale('pt-br');
  }

  getMaioresTransacoes(lancamentos) {
    this.maioresTransacoes = lancamentos.sort((a, b) => ((a.valor > b.valor) ? 1 : -1)).slice(0, 5);
  }

  getTransacoesPendentes(lancamentos) {
    const hoje = new Date();
    this.transacoesPendentes = lancamentos.filter(transacao => transacao.data < hoje && transacao.efetivado === false);
  }

  graficoPizzaHasValue() {
    let value = 0;
    this.dadosGraficoPizza.forEach(dado => value += dado.value);

    return value > 0;
  }

  somaTotais(lancamentos) {

    this.somaEntradas = 0;
    this.somaSaidas = 0;

    lancamentos.forEach(transacao => {
      if (transacao.valor > 0) {
        this.somaEntradas += transacao.valor;
      } else {
        this.somaSaidas += transacao.valor;
      }
    });

    this.somaSaidas = Math.abs(this.somaSaidas);
  }

  getValorAbs(valor) {
    return Math.abs(valor);
  }

  getCategoriaName(categoria): string {
    return Transacao.CategoriasLabels[categoria] || 'Receita';
  }

  getCategoriaIcon(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].icon : 'attach_money';
  }

  getCategoriaColor(categoria): string {
    return Transacao.CategoriasIcons[categoria] ? Transacao.CategoriasIcons[categoria].color : '#4bb35f';
  }

  getCategoriaPercent(value): number {
    return Math.abs(value / this.totalCategorias);
  }

  somaCategorias(transacoes) {
    this.somasCategorias = [];
      this.dadosGraficoPizza = [];
      this.dadosGraficoCategorias = [];
      this.totalCategorias = 0;
      this.receitaTotal = 0;
      this.transacoesPorCategoria = {};

      // receitas
      transacoes.forEach(t => {
        if (t.valor > 0) { this.receitaTotal += t.valor; }
      });

      Transacao.getCategorias().forEach(c => {
        let valSoma = 0;
        const itens = [];
        transacoes.forEach(t => {
          if (t.categoria === c) {
            valSoma += t.valor;
            itens.push(t);
          }
        });

        this.totalCategorias += Math.abs(valSoma);
        this.transacoesPorCategoria[c] = itens;
        this.dadosGraficoPizza.push({ 'name': Transacao.getCategoriaName(c), 'value': Math.abs(valSoma) });
        this.dadosGraficoCategorias[c] = Math.round(Math.abs(valSoma / (this.receitaTotal === 0 ? 1 : this.receitaTotal)) * 100);
        this.somasCategorias.push({ categoria: c, soma: Math.abs(valSoma)});
      });

      this.somasCategorias = this.somasCategorias.sort((a, b) => ((a.soma < b.soma) ? 1 : -1));
      this.maiorCategoria = this.somasCategorias[0];
  }

  getTodayDateString() {
    const dateToShow = moment();
    return `${moment.weekdays()[dateToShow.weekday()]}, ${dateToShow.date()} de ${
      moment.months()[dateToShow.month()]} de ${dateToShow.year()}`;
  }

  getCurrentMonth() {
    return `${moment.months()[moment().month()]}`;
  }

  getWeekItems() {

  }

}
