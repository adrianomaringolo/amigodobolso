import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { MzToastService } from 'ngx-materialize';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import 'rxjs/add/observable/of';

interface User {
  uid: string;
  firstName?: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  providerData?: any[];
}

@Injectable()
export class AuthService {

  user: Observable<User>;
  loggedUser: User;
  private userDetails: firebase.User = null;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router,
    private _toastService: MzToastService) {

    //// Get auth data, then get firestore user document || null
    this.user = this.afAuth.authState
    .pipe(
      switchMap(user => {
        if (user) {
          this.loggedUser = user;
          this.loggedUser.firstName = user.displayName.split(' ')[0];
          return this.afs.doc<User>(`usuarios/${user.uid}`).valueChanges();
        } else {
          return Observable.of(null);
        }
      })
    );
  }

  googleLogin() {
    return this.oAuthLogin(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        console.log('Logged in');
        this.router.navigate(['/inicio']);
      })
      .catch((err) => {
        console.log(err);
        if (err['code'] === 'auth/account-exists-with-different-credential') {
          this._toastService.show(`O email "${err['email']}" está associado ao login pelo Facebook.<br/>
            Por favor, tente novamente por esse serviço`, 10000, 'message__error');
        }

      });
  }

  // signInWithFacebook() {
  //   return this.afAuth.auth.signInWithPopup(
  //     new firebase.auth.FacebookAuthProvider()
  //   )
  // }

  facebookLogin() {

    return this.oAuthLogin(new firebase.auth.FacebookAuthProvider())
    .then(res => {
      console.log('Logged in');
      this.router.navigate(['/inicio']);
    })
    .catch((err) => {
      console.log(err);
      if (err['code'] === 'auth/account-exists-with-different-credential') {
        this._toastService.show(`O email "${err['email']}" está associado ao login pelo Google.<br/>
          Por favor, tente novamente por esse serviço`, 10000, 'message__error');
      }
    });
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user);
      });
  }


  private updateUserData(user) {
    // Sets user data to firestore on login

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`usuarios/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };

    return userRef.set(data, { merge: true });
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }
}
