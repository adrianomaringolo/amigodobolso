import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {

  public showLoading = false;

  constructor() { }

  public startLoading() {
    this.showLoading = true;
  }

  public stopLoading() {
    this.showLoading = false;
  }

}
