import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from '../app.routes';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { FormsModule } from '@angular/forms';

import { AddButtonComponent } from './components/add-button/add-button.component';
import { MzSelectModule, MzDatepickerModule, MzInputModule,
  MzToastModule, MzCollectionModule, MzSidenavModule, MzDropdownModule,
  MzModalModule, MzButtonModule, MzTabModule, MzCollapsibleModule,
  MzFeatureDiscoveryModule, MzTextareaModule, MzCheckboxModule, MzTooltipModule, MzCardModule } from 'ngx-materialize';

import { AuthService } from './services/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { LoginButtonComponent } from './components/login-button/login-button.component';
import { LoadingComponent } from './components/loading/loading.component';
import { LoadingService } from './services/loading.service';
import { NavigationService } from './services/navigation.service';
import { MonthpickerComponent } from './components/monthpicker/monthpicker.component';
import { NgSelectModule } from '@ng-select/ng-select';

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FilterbarComponent } from './components/filterbar/filterbar.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgSelectModule,
    FormsModule,
    CurrencyMaskModule,
    MzSelectModule,
    MzDatepickerModule,
    MzInputModule,
    MzToastModule,
    MzCollectionModule,
    MzSidenavModule,
    MzDropdownModule,
    MzModalModule,
    MzButtonModule,
    MzTabModule,
    MzFeatureDiscoveryModule,
    MzCollapsibleModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    NgxSmartModalModule.forRoot(),
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-popup confirm',
      cancelButtonClass: 'btn btn-popup cancel'
  })
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    AddButtonComponent,
    LoginButtonComponent,
    LoadingComponent,
    MonthpickerComponent,
    FilterbarComponent
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    AddButtonComponent,
    CommonModule,
    NgSelectModule,
    FormsModule,
    CurrencyMaskModule,
    MzSelectModule,
    MzDatepickerModule,
    MzInputModule,
    MzToastModule,
    MzCollectionModule,
    MzSidenavModule,
    MzDropdownModule,
    MzModalModule,
    MzButtonModule,
    MzTabModule,
    MzFeatureDiscoveryModule,
    MzCollapsibleModule,
    MzTextareaModule,
    MzCheckboxModule,
    MzTooltipModule,
    MzCardModule,
    LoginButtonComponent,
    LoadingComponent,
    MonthpickerComponent,
    NgxSmartModalModule,
    SweetAlert2Module
  ],
  providers: [
    AuthService,
    LoadingService,
    NavigationService
  ]
})
export class SharedModule { }
