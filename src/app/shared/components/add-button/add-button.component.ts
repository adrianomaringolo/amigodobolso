import { Component, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { MzFeatureDiscoveryComponent } from 'ngx-materialize';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html'
})
export class AddButtonComponent implements AfterViewChecked {

  @ViewChild('featureDiscovery') featureDiscovery: MzFeatureDiscoveryComponent;

  constructor() {
  }

  ngAfterViewChecked() {
    // this.featureDiscovery.open();
  }

}
