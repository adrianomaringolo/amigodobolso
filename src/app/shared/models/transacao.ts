import { Usuario } from './usuario';

export class Transacao {
  static readonly CategoriasLabels = {
    necessidades_essenciais: 'Necessidades Essenciais',
    tranquilidade_financeira: 'Tranquilidade Financeira',
    fazer_pelo_outro: 'Fazer pelo Outro',
    diversao: 'Diversão',
    compras_longo_prazo: 'Compras de Longo Prazo',
    desenvolvimento_pessoal: 'Desenvolvimento Pessoal'
  };

  static readonly CategoriasIcons = {
    necessidades_essenciais: { icon: 'shopping_basket', color: '#8db4e3' },
    tranquilidade_financeira: { icon: 'beach_access', color: '#eb892c' },
    fazer_pelo_outro: { icon: 'favorite', color: '#953737' },
    diversao: { icon: 'tag_faces', color: '#d43826' },
    compras_longo_prazo: { icon: 'access_time', color: '#17375c' },
    desenvolvimento_pessoal: { icon: 'school', color: '#262a2b' }
  };

  static readonly Categorias = {
    NecessidadesEssenciais: 'necessidades_essenciais',
    TranquilidadeFinanceira: 'tranquilidade_financeira',
    ComprasDeLongoPrazo: 'compras_longo_prazo',
    FazerPeloOutro: 'fazer_pelo_outro',
    Diversao: 'diversao',
    DesenvolvimentoPessoal: 'desenvolvimento_pessoal'
  };

  public id?: string;
  public valor: number;
  public descricao: string;
  public data: Date;
  public forma: string;
  public categoria: string;
  public numParcelas: number;
  public periodo: string;
  public parcela: number;
  public obs?: string;
  public efetivado: boolean;

  public usuario: Usuario;

  static getCategoriaName(categoria): string {
    return Transacao.CategoriasLabels[categoria];
  }

  static getCategorias(): string[] {
    return Object.keys(Transacao.CategoriasLabels);
  }


  constructor() {}
}
