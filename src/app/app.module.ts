import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from './shared/shared.module';
import { TransacoesModule } from './transacoes/transacoes.module';
import { routing } from './app.routes';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CategoriaDetailsComponent } from './relatorio/categoria-details/categoria-details.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthGuard } from './shared/auth.guard';

import {
  AuthMethods,
  AuthProvider,
  AuthProviderWithCustomConfig,
  CredentialHelper,
  FirebaseUIAuthConfig,
  FirebaseUIModule
} from 'firebaseui-angular';

import localePT from '@angular/common/locales/pt';
import { HelpComponent } from './help/help.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { PoliticaPrivacidadeComponent } from './help/politica-privacidade/politica-privacidade.component';
import { FuncionalidadesComponent } from './help/funcionalidades/funcionalidades.component';
registerLocaleData(localePT);

const facebookCustomConfig: AuthProviderWithCustomConfig = {
  provider: AuthProvider.Facebook,
  customConfig: {
    scopes: [
      'public_profile',
      'email'
    ],
    customParameters: {
      // Forces password re-entry.
      //auth_type: 'reauthenticate'
    }
  }
};

const firebaseUiAuthConfig: FirebaseUIAuthConfig = {
  providers: [
    AuthProvider.Google,
    AuthProvider.Facebook,
    AuthProvider.Password,
  ],
  method: AuthMethods.Popup,
  tos: '<your-tos-link>',
  credentialHelper: CredentialHelper.AccountChooser,
  autoUpgradeAnonymousUsers: true,
  disableSignInSuccessCallback: true
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RelatorioComponent,
    HelpComponent,
    CategoriaDetailsComponent,
    UserProfileComponent,
    PoliticaPrivacidadeComponent,
    FuncionalidadesComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    BrowserAnimationsModule,
    TransacoesModule,
    routing,
    HttpClientModule,
    NgxChartsModule,
    AngularFireModule.initializeApp(environment.firebase),
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    AngularFirestoreModule.enablePersistence(),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'pt' }, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
