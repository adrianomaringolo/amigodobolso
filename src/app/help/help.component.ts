import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../shared/services/navigation.service';
import { Transacao } from '../shared/models/transacao';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  CategoriasIcons = Transacao.CategoriasIcons;

  constructor(private _navService: NavigationService) {
    this._navService.isInList = false;
  }

  ngOnInit() {
  }

}
